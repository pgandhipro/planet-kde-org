# 🌎 Planet KDE

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_planet-kde-org)](https://binary-factory.kde.org/job/Website_planet-kde-org/)

[Planet KDE](https://planet.kde.org) is a web feed aggregator that collects blog posts from people who contribute to KDE.

If you are a KDE contributor you can have your blog on Planet KDE. Blog content should be mostly
KDE themed and not liable to offend. If you have a general blog you may want to set up a tag and
subscribe the feed for that tag only to Planet KDE.

## Adding your feed

Anyone in the KDE group on invent.kde.org can add new feeds to this repository.

If you want to get your feed added, we prefer Merge Requests via [invent.kde.org](https://invent.kde.org/websites/planet-kde-org).

- Fork this repository
- Edit [planet.ini](https://invent.kde.org/websites/planet-kde-org/tree/master/planet.ini) and add:

```ini
[id]       # replace id with your feed's unique identifier (a-z0-9-_) (e.g. kde-dot)
feed_url = # url to your rss/atom feed                                (e.g. https://dot.kde.org/rss.xml)
title    = # (optional) title of your feed                            (e.g. KDE Dot)
           #   will be used as the `author` field of a post
           #   so better set this to your name for people to identify you
           #   if not set, the title field in the feed data will be used
site_url = # (optional) url to your website                           (e.g. https://dot.kde.org)
           #   default to the link field in the feed data
           #   or if that's not set, the host part of feed_url 
lang     = # (optional) feed's language code, check [1]
           #   default to 'en'
avatar   = # (optional) filename or url of your avatar                (e.g. kde.png)
flairs   = # (optional) space-separated list of flairs of the author, currently supports:
           #   irc:irc_nickname          (e.g. irc:myircnickname)
           #   matrix:@username:url      (e.g. matrix:@dot:kde.org)
           #   telegram:username         (e.g. telegram:dotkde)
           #   sok
           #   gsoc
discuss  = # (optional) a number to tell whether this feed's posts should be published on [Discuss](discuss.kde.org)
           #   (default) 0: no
           #   not 0: yes
```
\[1]: https://invent.kde.org/websites/hugo-i18n/-/blob/master/hugoi18n/resources/languages.yaml  
It has been agreed that both Portuguese and Brazilian Portuguese feeds should use the `pt-pt` code.  

- Upload your avatar to [static/hackergotchi directory](https://invent.kde.org/websites/planet-kde-org/tree/master/static/hackergotchi)
- Send a Pull Request

If you do not have a Git account, [file a bug in Bugzilla](https://bugs.kde.org/enter_bug.cgi?product=planet%20kde) listing your name, Git account (if you have one), IRC nick (if you have one), RSS or Atom feed and what you do in KDE. Attach a photo of your face for hackergotchi.

## Planet KDE Guidelines

Planet KDE is one of the public faces of the KDE project and is read by millions of users and potential
contributors. The content aggregated at Planet KDE is the opinions of its authors, but the sum of that
content gives an impression of the project. Please keep in mind the following guidelines for your blog
content and read the [KDE Code of Conduct](https://kde.org/code-of-conduct/). The KDE project
reserves the right to remove an inappropriate blog from the Planet. If that happens multiple times, the
Community Working Group can be asked to consider what needs to happen to get your blog aggregated again.

If you are unsure or have queries about what is appropriate contact the KDE Community Working Group.

### Blogs should be KDE themed

The majority of content in your blog should be about KDE and your work on KDE. Blog posts about personal
subjects are also encouraged since Planet KDE is a chance to learn more about the developers behind KDE.
However blog feeds should not be entirely personal, if in doubt set up a tag for Planet KDE and subscribe
the feed from that tag so you can control what gets posted.

### Posts should be constructive

Posts can be positive and promote KDE, they can be constructive and lay out issues which need to be
addressed, but blog feeds should not contain useless, destructive and negative material. Constructive
criticism is welcome and the occasional rant is understandable, but a feed where every post is critical
and negative is unsuitable. This helps to keep KDE overall a happy project.

### You must be a KDE contributor

Only have your blog on Planet KDE if you actively contribute to KDE, for example through code, user
support, documentation etc.

### It must be a personal blog, or in a blog class

Planet KDE is a collection of blogs from KDE contributors.

### Do not inflame

KDE covers a wide variety of people and cultures. Profanities, prejudice, lewd comments and content
likely to offend are to be avoided. Do not make personal attacks or attacks against other projects on
your blog.

For further guidance on good practice see the [KDE Code of Conduct](https://kde.org/code-of-conduct/).

## Development environment
- As a (Hu)Go module, this website requires both Hugo and Go to work;
- `feedparser` Python package is required to parse feeds;
- `hugoi18n` is needed for I18n. See [the project repo](https://invent.kde.org/websites/hugo-i18n).

Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

To run this website locally, after having those requirements and the shared theme set up, use the following commands:

```sh
python3 scripts/custom_generation.py
# run the development server
hugo server
```

and visit [http://localhost:1313](http://localhost:1313).
